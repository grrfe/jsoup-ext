plugins {
    java
    `maven-publish`
    kotlin("jvm") version "1.8.21"
    id("net.nemerosa.versioning") version "3.1.0"
}

group = "fe.jsoup.ext"
version = versioning.info.tag ?: versioning.info.full

repositories {
    mavenCentral()
}

dependencies {
    api(platform("com.github.1fexd:super"))
    api("org.jsoup:jsoup")

    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = project.group.toString()
            version = project.version.toString()

            from(components["java"])
        }
    }
}
