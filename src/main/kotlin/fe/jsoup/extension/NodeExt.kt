package fe.jsoup.extension

import org.jsoup.nodes.Node

fun Node.src(): String = attr("src")
fun Node.href(): String = attr("href")
