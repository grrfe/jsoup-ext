package fe.jsoup.extension

import org.jsoup.nodes.Element
import org.jsoup.select.Elements

fun Element.firstByTagOrNull(tagName: String): Element? {
    return getElementsByTag(tagName).firstOrNull()
}

fun Element.firstByClassOrNull(className: String): Element? {
    return getElementsByClass(className).firstOrNull()
}

fun Element.firstChildOrNull(): Element? {
    return if (childrenSize() > 0) child(0) else null
}

fun Element.singleByClassOrNull(className: String): Element? {
    val elements = getElementsByClass(className)
    return if (elements.size == 1) elements[0] else null
}

fun Element.byTag(tagName: String): Elements {
    return getElementsByTag(tagName)
}

fun Element.byIdOrNull(id: String): Element? {
    return getElementById(id)
}
